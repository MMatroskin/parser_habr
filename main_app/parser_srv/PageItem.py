from bs4 import BeautifulSoup
from datetime import datetime
import re
from shared_items.ReportItem import ReportItem


class PageItem:
    _content = None
    _sep_h = '\n'
    _sep_w = '\t'
    _list_item_marker = '\t'
    url = None
    data = dict()

    def __init__(self, content, data_items, **data):

        url = data.get('url')
        self.success = ReportItem(url)
        self._host = data.get('host')
        try:
            soup = BeautifulSoup(content, 'html.parser')
            self._content = soup.find('article', {'class': 'post_full'})
        except Exception:
            pass
        if self._content is not None:
            self.data = dict.fromkeys(data_items, '')
            self.data['name'] = self._content.find('span', {'class': 'post__title-text'}).text
            self.data['url'] = url
            self.data['user'] = self.get_user_info(soup)
        else:
            self.success.success = False
            self.success.message = 'Data empty'

    def parse_content(self):
        try:
            data_container = self._content.find(lambda tag:
                                                   tag.name == 'span' and
                                                   tag.attrs.get('class') == ['post__time'])
            if data_container is not None:
                try:
                    date = datetime.strptime(data_container.attrs['data-time_published'][0:-1], '%Y-%m-%dT%H:%M')
                    self.data['date'] = date
                except Exception:
                    pass

            hubs_container = self._content.find('ul', {'class': 'post__hubs_full-post'})
            if hubs_container is not None:
                hubs_items = self._get_list_content(hubs_container)
                self.data['hubs'] = self._sep_w.join(hubs_items)

            types_container = self._content.find('ul', {'class': 'post__marks'})
            if types_container is not None:
                types_items = self._get_list_content(types_container)
                self.data['types'] = self._sep_w.join(types_items)

            tags_container = self._content.find(lambda tag:
                                                   tag.attrs.get('class') == ['post__tags'])
            if tags_container is not None:
                tags_items = self._get_list_content(tags_container)
                self.data['tags'] = self._sep_w.join(tags_items)

            img_items = self._content.findAll('img')
            img_links = []
            for i in img_items:
                img_links.append(i.attrs['src'])
            self.data['images'] = img_links

            content_container = self._content.find(lambda tag:
                                                 tag.name == 'div' and
                                                 tag.attrs.get('id') == 'post-content-body')
            if content_container is not None:
                content_data = []
                content_items = content_container.contents
                for i in content_items:
                    line = None
                    try:
                        line = self._normalize_text(i)
                    except Exception:
                        try:
                            if i.name == 'img':
                                line = 'Image: ' + i.attrs['src']
                            elif i.name == 'ul':
                                tmp = self._get_list_content(i, add_marker=True)
                                line = self._sep_h.join(tmp)
                            else:
                                table_container = i
                                if table_container.name != 'table':
                                    table_container = i.find('table')
                                if table_container is not None:
                                    tmp = self._get_table_content(table_container)
                                    line = self._sep_h.join(tmp)
                                else:
                                    line = self._normalize_text(i.text)
                        except Exception:
                            pass
                    if line is not None and len(line) > 0:
                        content_data.append(line)
                self.data['content'] = self._sep_h.join(content_data)
        except Exception as e:
            self.success.success = False
            self.success.message = str(e)

    def _get_list_content(self, container, add_marker=False):
        result = []
        if container.name == 'ul':
            item = container
        else:
            item = container.find('ul')
        if item is not None:
            for i in item.contents:
                line = ''
                try:
                    line = self._normalize_text(i.text)
                except Exception:
                    pass
                if len(line) > 0:
                    if add_marker:
                        line = self._list_item_marker + line
                    result.append(line)
        return result

    def _get_table_content(self, item, is_horizontal=False):
        result = []
        try:
            tmp = []
            subitems = item.findAll('tr')
            for i in subitems:
                value = []
                for j in i.contents:
                    if j.name == 'td' or j.name == 'th':
                        value.append(self._normalize_text(j.text))
                if len(value) > 0:
                    tmp.append(value)
            sep_w = self._sep_w
            if is_horizontal:
                for i in range(len(tmp[0])):
                    items = []
                    for j in range(len(tmp)):
                        items.append(tmp[j][i])
                    result.append(sep_w.join(items))
            else:
                for i in range(len(tmp)):
                    result.append(sep_w.join(tmp[i]))
        except Exception:
            pass
        return result

    def _normalize_text(self, line):
        result = re.sub('\t{2,}', '\t', line)
        result = re.sub('\n{2,}', '\n', result)
        result = self._clear_line(result)
        return result

    @staticmethod
    def _clear_line(line, item=','):
        result = line.strip()
        if result.endswith(item):
            result = result[0: len(result) - 1]
        result = result.strip()
        return result

    def get_user_info(self, data):
        result = {}
        user_info_container = data.find('div', {'class': 'user-info__about'})
        if user_info_container is not None:
            user_name_item = user_info_container.find('a', {'class': 'user-info__fullname'})
            if user_name_item is not None:
                try:
                    result['name'] = user_name_item.text
                except Exception:
                    pass
            user_nick_item = user_info_container.find('a', {'class': 'user-info__nickname'})
            if user_nick_item is not None:
                try:
                    result['nick'] = user_nick_item.text
                    result['url'] = self._host + user_nick_item.attrs['href']
                except Exception:
                    pass
        return result
