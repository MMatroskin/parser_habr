import requests
import aiohttp
import asyncio
from .Result import Result


def get_html(url, headers):
    res = requests.get(url, headers=headers, params=None)
    result = Result()
    result.url = res.url
    result.status = res.status_code
    if result.status == 200:
        result.data = res.text
    return result


def get_html_async(url_list, headers):
    loop = asyncio.get_event_loop()
    coroutines = [get(i, headers) for i in url_list]
    result = loop.run_until_complete(asyncio.gather(*coroutines))
    return result


async def get(url, headers=None):
    async with aiohttp.ClientSession(headers=headers) as s:
        async with s.get(url) as res:
            result = Result()
            result.url = url
            result.status = res.status
            if result.status == 200:
                result.data = await res.content.read()
            return result
