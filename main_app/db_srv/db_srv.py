import sqlite3
import os
from shared_items.ReportItem import ReportItem


class Database(object):

    def __new__(cls,  name):
        if not hasattr(cls, 'instance'):
            cls.instance = super(Database, cls).__new__(cls)
            current_dir = os.getcwd()
            parent_dir = os.path.join(current_dir, os.pardir)
            cls.instance.db_name = name + '.sqlite'
            cls.instance.db_dir = os.path.abspath(parent_dir)
            if not os.path.exists(os.path.join(cls.instance.db_dir, cls.instance.db_name)):
                cls.instance._create_db()
        return cls.instance

    def _create_db(self):
        try:
            db_full_name = os.path.join(self.db_dir, self.db_name)
            conn = sqlite3.connect(db_full_name)
            cursor = conn.cursor()
            sql = """
                begin;
                pragma foreign_keys=on;
                create table users(
                    id integer primary key autoincrement,
                    nick text unique,
                    name text,
                    url text
                );
                create table articles(
                    id integer primary key autoincrement,
                    name text,
                    url text unique,
                    date datetime,
                    user_id integer not null,
                    tags text,
                    types text,
                    hubs text,
                    content text,
                    images text,
                    foreign key (user_id) references users(id)
                );
                commit;
            """
            cursor.executescript(sql)
            conn.commit()
            conn.close()
        except Exception as e:
            print(str(e))

    def _get_user_id(self, user):
        try:
            sql_get_id = "select id from users as u where u.nick like '" + user.get('nick') + "'"
            conn = sqlite3.connect(os.path.join(self.db_dir, self.db_name))
            cursor = conn.cursor()
            cursor.execute(sql_get_id)
            sql_result = cursor.fetchall()
            if len(sql_result) == 0:
                values = [user.get('nick'), user.get('name'), user.get('url')]
                sql_add_user = "insert into users values(null, ?, ?, ?)"
                cursor.execute(sql_add_user, values)
                conn.commit()
                cursor.execute(sql_get_id)
                sql_result = cursor.fetchall()
                conn.close()
            result = sql_result[0][0]
        except Exception:
            result = None
        return result

    def add_data(self, data_items, data):
        result = ReportItem(data.get('name'))
        user = data.get('user')
        try:
            user_id = self._get_user_id(user)
            if user_id is not None:
                values = []
                for i in data_items:
                    if i == 'images':
                        val = ' '.join(data.get(i))
                    elif i == 'user':
                        val = user_id
                    else:
                        val = data.get(i)
                    values.append(val)
                sql = "insert into articles values(null, ? ,?, ?, ?, ?, ?, ?, ?, ?)"
                conn = sqlite3.connect(os.path.join(self.db_dir, self.db_name))
                cursor = conn.cursor()
                cursor.execute(sql, values)
                conn.commit()
                conn.close()
        except Exception as e:
            result.success = False
            result.message = str(e)
        return result

    def get_articles(self):
        sql = """
            select 
                users.nick,
                users.name,
                articles.name,
                articles.url,
                articles.date,
                articles.tags,
	            articles.types,
                articles.hubs,
                articles.content
            from articles
            inner join users
            on articles.user_id = users.id
            order by users.nick
        """
        try:
            conn = sqlite3.connect(os.path.join(self.db_dir, self.db_name))
            cursor = conn.cursor()
            cursor.execute(sql)
            result = cursor.fetchall()
        except Exception as e:
            result = None
            print(str(e))
        return result
