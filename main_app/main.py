import schedule
import time
from parse_app import starter, check
from config import FIRST_TASK_TIME, NEXT_TASK_TIME, CHECK_TIME


def job(name=None):
    msg = 'Task:'
    section = None
    if name is None:
        name = 'Next'
        section = 'src_list'
    print(name, msg, sep=' ')
    starter(section=section)
    print()


def main():
    global cont
    cont = True
    print('Task scheduler started, press Ctrl+C / Ctrl+Break for quit\n')
    schedule.every().day.at(FIRST_TASK_TIME).do(job, name='First')
    schedule.every().day.at(NEXT_TASK_TIME).do(job)
    schedule.every().day.at(CHECK_TIME).do(check)

    try:
        while True:
            schedule.run_pending()
            time.sleep(1)
    except KeyboardInterrupt:
        print('Done')


if __name__ == '__main__':
    main()
