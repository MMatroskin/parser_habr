import json
from config import *
from parser_srv.PageItem import PageItem
from request_srv.RequestSrv import get_html_async
from db_srv.db_srv import Database


def parse_items(src_items, section):
    data_items = []
    link_errors = []
    error_items = []
    html_items = get_html_async(src_items.get(section), HEADERS)
    for i in html_items:
        if i.status == 200:
            item = PageItem(i.data, src_items.get('data_items'), url=i.url, host=src_items.get('host'))
            if item.success.success:
                item.parse_content()
                data_items.append(item.data)
            else:
                error_items.append(item.success)
        else:
            link_errors.append(i.url)
    return [data_items, link_errors, error_items]


def save_data(data_items, src_items):
    result = []
    db_srv_item = Database(DB_NAME)
    for i in data_items:
        result.append(db_srv_item.add_data(src_items.get('data_items'), i))
    return result


def get_src_data(section):
    result = None
    with open(SRC_DATA, 'r') as fh:
        src_items = json.load(fh)
    if src_items.get(section) is not None and src_items.get('data_items') is not None:
        result = src_items
    return result


def check():
    print('Check DB:')
    db_srv_item = Database(DB_NAME)
    result = db_srv_item.get_articles()
    for i in result:
        print(i)


def starter(section=None):
    if section is None:
        section = 'src_list_init'
    src_items = get_src_data(section=section)
    if src_items is None:
        print('Error in src data!')
    else:
        print('Processing..')
        res = parse_items(src_items, section)
        if len(res[1]) or len(res[2]) > 0:
            print('\nErrors:')
            for i in res[1]:
                print(i)
            for i in res[2]:
                print(i.info, i.message)
        result = save_data(res[0], src_items)
        for i in result:
            print(i.info, i.message)


if __name__ == '__main__':
    starter()
    print()
    check()
